//[SECTION] Dependencies and Modules
	const express = require("express"); 
	const mongoose = require("mongoose"); 

//[SECTION] Server Setup
	//establish a connection
	const app = express(); 
	//define a path/address
	const port = 4000; 
	//Assign the port 4000 to avoid conflict in the future with frontend applications that run on port 3000


//[SECTION] Database Connection
   //Connect to MongoDB Atlas
   //change the password bracket to your own database password
   //Get the credentials of your atlas user.
   mongoose.connect('mongodb+srv://teejae:admin123@batch73.ckfry.mongodb.net/toDo176?retryWrites=true&w=majority',{

   	//options to add to avoid deprecation warnings because of mongoose/mongodb update.
   	useNewUrlParser:true,
   	useUnifiedTopology:true

   });

   //Create notifications if the connection to the db is a success or a failure:
   let db = mongoose.connection;
   //Let's add an on() method from our mongoose connection to show if the connection has succeeded or failed in both the terminal and in the browser for our client.
   db.on('error',console.error.bind(console,"Connection Error"));
   //once the connection is open and successful, we will output a message in the terminal:
   db.once('open',()=>console.log("Connected to MongoDB"));

   //Middleware - A middleware, in expressjs context, are methods, functions that acts and adds features to our application.
   app.use(express.json());

//[SECTION] Entry Point Response
	//bind the connection to the designated port
	app.listen(port, () => console.log(`Server running at port ${port}`)); 




const	taskSchema =new mongoose.Schema({

	name: String,
	status:String
})

//Mongose Model
/*
	Models are use to connect your api to the corresponding collection i  our database. It is a represenation of our collection.

	Models uses schemas to create object that correspons to the schema. By default, when creating the collection from your model, the collection name is pluralized.

	moongose.model(<nameOfCollectionInAtlas>,<schemaToFollow>)
*/


const Task = mongoose.model("task", taskSchema);

//POst route to create a new task
app.post('/tasks',(req,res)=>{

	//when crating a new post/put or any route that requires data from the client first console log your req.body or any part of the request that contains the data.connection

	console.log(req.body);



//creating a new task document by using the contructor of our Task model. This constructor should followthe schema of the model

let newTask = new Task({
	name:req.body.name,
	status: req.body.status

})

//.save () method from a object created by a model
//save() method will allow us to save our document by connecting to our collection via model.

//save() has 2 approaches:
//1. we can add an anonymous function to handle the created document or error.
//2. we can add .then() chain which will allow us the handle errors and created documents in separate functions.

newTask.save((savedTask,error)=> {
	if (error){
		res.send(error);
	}else{
		res.send(savedTask)
	}
	})
})






//[SECTION] Entry Point Response
	//bind the connection to the designated port
app.listen(port,()=>console.log(`Server running at port ${port}`));


const Sample = mongoose.model("samples",sampleSchema);

app.post(('/samples',(req,res)=> {

let newSample = new Sample({

	name:req.body,name,
	isActive:req.body.isActive
})

newSample.save((savedSample,error)=>{
	if(error){
		res.send(error);
		}
})
}


